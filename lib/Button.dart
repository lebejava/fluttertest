import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_actor.dart';

class Button extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new InkWell(
      child: Container(
        margin: new EdgeInsets.only(
          top: 20.0
        ),
        height: 150.0,
        width: 150.0,
        decoration: new BoxDecoration(
          boxShadow: [
            new BoxShadow(
              color: Color(0x00000000),
              offset: new Offset(10.0, 10.0),
              blurRadius: 30.0
            ),
          ]
        ),
        child: FlareActor("assets/buttonok.flr", animation: "Untitled"))
    );
  }

}