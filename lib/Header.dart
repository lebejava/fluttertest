import 'package:flutter/material.dart';
import 'Background.dart';
import 'Button.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        children: <Widget>[
          new Background(),
          new Container(
            alignment: Alignment.center,
            margin: new EdgeInsets.only(top: 50.0),
            child: new Column(
              children: <Widget>[
                new Text(
                  "NETFLIX",
                  style: const TextStyle(
                      fontSize: 55.0,
                      color: Color(0x80FFFFFF),
                      fontWeight: FontWeight.w600),
                ),
                new Button()
              ],
            ),
          )
        ],
      ),
    );
  }
}
